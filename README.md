# README

## Requirements

Install Docker https://docs.docker.com/engine/install/

## Run application

    cp .env.example .env
    docker compose up 

- Open http://localhost:3000/inspect/http & get your external domain like `https://SOME_NAME.eu.ngrok.io`
- Sign up - https://ngrok.com/signup & verify your email
- Get Authtoken https://dashboard.ngrok.com/get-started/your-authtoken